#!/usr/bin/env python

import setuptools

setuptools.setup(
    name='vanPuttenWaveform',
    version='1.1',
    description='simple waveform generator for f(t) from arXiv:1806.02165',
    author='David Keitel',
    author_email='david.keitel@ligo.org',
    url='https://git.ligo.org/david-keitel/vanPuttenWaveform',
    packages=setuptools.find_packages(),
    scripts=['scripts/makeVanPuttenWaveformData'],
    license='MIT License',
   )
