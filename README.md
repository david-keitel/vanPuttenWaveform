Simple waveform generator (and some more helper functions)
assuming the time-frequency evolution from
[arXiv:1806.02165](https://www.arxiv.org/abs/1806.02165)
and a simple neutron star spindown model
with fixed ellipticity and moment of inertia.

To install this package, do
```
python setup.py install --prefix=~/.local/
```
or similar from a local clone,
or you can also use pip to directly install from this repo:
```
pip install git+ssh://git@git.ligo.org/david-keitel/vanPuttenWaveform
```
(add --user if appropriate).

You should then be able to do
```
import vanPuttenWaveform
```
from within python, or run the makeVanPuttenWaveformData script
from the commandline.

The script however requires a reasonably recent version of LALSuite
that includes the `simulateCW` module;
versions from OS repos might be too old but
current versions from pip will do.

Example commandline to make 32s pure-signal frame files (H1+L1)
with van Putten's waveform parameters as default,
optimistic orientation and at a much closer distance of 0.1Mpc to make it stronger,
and some standard-ish ellipticity and moment of inertia.
This will take several minutes because of the fine required dtwf sampling:
```
./makeVanPuttenWaveformData  --T 32 --frames --frame-fs 16384 --dtwf=0.01 --dd=0.1 --II=2.05e45 --eps=0.01 --cosi=1 
```