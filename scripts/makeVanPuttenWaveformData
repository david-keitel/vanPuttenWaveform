#!/usr/bin/env python

'''
Copyright (C) 2018 David Keitel, Miquel Oliver

wrapper to generate frame/SFT files
with gravitational waveforms following
the 'exponential anti-chirp' track
suggested by M. van Putten in https://www.arxiv.org/abs/1806.02165
code based on the the millisecond magnetar model wrapper
implementation by Karl Wette
'''

from __future__ import division, print_function

import os
import sys
import argparse
import numpy as np
import datetime
import itertools
import multiprocessing
from copy import copy
from time import time

import lal
from lalpulsar.simulateCW import CWSimulator
from vanPuttenWaveform import waveform

from sys import argv
cmdline = ' '.join(argv)

parser = argparse.ArgumentParser()
parser.add_argument('--threads', help='number of parallel threads', type=int, default=4)
parser.add_argument('--t0', help='data start time [GPS]', type=float, default=1187008880.)
parser.add_argument('--T', help='data duration [s]', type=float, required=True)
parser.add_argument('--tref', help='signal start time [GPS]', type=float, default=1187008882.43)
# note on default sky position (for GW170817):
# taken from https://wiki.ligo.org/Main/RemnantGW170817#Sky_Direciton
# where alpha (RA) is in *hour angle*, not degree, so dividing by 12 not 180!
parser.add_argument('--alpha', help='signal right ascension [rad]', type=float, default=13.1634 * np.pi / 12.)
parser.add_argument('--delta', help='signal declination [rad]', type=float, default=-23.3815 * np.pi / 180.)
parser.add_argument('--phi0', help='signal initial phase [rad]', type=float, default=0.0)
parser.add_argument('--psi', help='signal polarisation angle [rad]', type=float, default=0.0)
parser.add_argument('--f0', help='asypmtotic GW frequency at end of exponential track [Hz]', type=float, nargs='*', default=[98.])
parser.add_argument('--ts', help='start time of exponential track (after coalescence) [s]', type=float, nargs='*', default=[0.67])
parser.add_argument('--fs', help='starting frequency of exponential track [Hz]', type=float, nargs='*', default=[650.])
parser.add_argument('--tau', help='e-fold spindown timescale [s]', type=float, nargs='*', default=[3.01])
parser.add_argument('--eps', help='stellar ellipticity', type=float, nargs='*', default=[0.01])
parser.add_argument('--dd', help='distance [Mpc]', type=float, nargs='*', default=[40.0])
parser.add_argument('--cosi', help='cosine of inclination', type=float, nargs='*', default=[0.0])
parser.add_argument('--II', help='moment of inertia [g cm^2]', type=float, default=1.e45)
parser.add_argument('--detectors', help='detectors', type=str, nargs='*', default=['H1', 'L1'])
parser.add_argument('--noise-sqrt-Sh', help='add Gaussian noise with this sqrt-Sh', type=float, default=0.)
parser.add_argument('--frames', help='make frames?', action='store_true')
parser.add_argument('--frame-fs', help='frame sampling frequency [Hz]', type=int, default=8192)
parser.add_argument('--frame-T', help='frame duration [s]', type=int, default=16)
parser.add_argument('--sfts', help='make SFTs?', action='store_true')
parser.add_argument('--sft-fmax', help='SFT maximum frequency [Hz]', type=int, default=2048)
parser.add_argument('--sft-T', help='SFT timebase [s]', type=int, default=1)
parser.add_argument('--force', help='force SFT/frame file regeneration even if matching old files found', action='store_true')
parser.add_argument('--outdir', help='base directory for writing frame+SFT files', type=str, default='')
parser.add_argument('--dtwf', help='signal sampling rate [Hz]', type=float, default=0.5)
parser.add_argument('--sft-window', help='window function name for SFTs', type=str, default=None)
parser.add_argument('--sft-window-param', help='optional parameter to some SFT window functions', type=float, default=0.)

try:
    args = parser.parse_args()
except:
    sys.exit(1)
force_rerun = args.force

# function to write out a per-param-combination argument string
def get_args_str ( args, param, detector ):
    args_p = copy(args)
    (args_p.f0, args_p.ts, args_p.fs, args_p.tau, args_p.eps, args_p.dd, args_p.cosi) = param
    args_p.detectors = detector
    args_str = ', '.join(['%s=%s' % (arg, getattr(args_p, arg)) for arg in vars(args_p)])
    return args_str

# function which makes waveform data
def make_van_putten_data(param):
    (f0, ts, fs, tau, eps, dd, cosi) = param

    for detector in args.detectors:

        t0 = time()

        # waveform data directory
        param_str = 'f0-%g_ts-%g_fs-%g_tau-%g_eps-%g_dd-%g_cosi-%g' % (f0, ts, fs, tau, eps, dd, cosi)
        out_dir = os.path.join(args.outdir, param_str)
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        out_dir = os.path.join(out_dir, detector)
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        # exit if this data has already been generated
        args_str = get_args_str ( args, param, detector )
        stamp_file_name = 'FINISHED.txt'
        stamp_file = os.path.join(out_dir, stamp_file_name)
        if not force_rerun and os.path.isfile(stamp_file):
            with open(stamp_file) as f:
                flines = f.readlines()
                if flines[-1].strip('\n') == args_str:
                    print('%s: nothing to do' % (out_dir))
                    continue
                else:
                    print('%s: found previous "%s" file but used arguments mismatch, regenerating now!' % (out_dir,stamp_file_name))

        # waveform model
        wf = waveform(f0, ts, fs, tau, eps, dd, cosi, args.II)

        # simulate signal
        t1 = time()
        S = CWSimulator ( tref=args.tref,
                          tstart=args.t0,
                          Tdata=args.T,
                          waveform=wf,
                          dt_wf=args.dtwf,
                          phi0=args.phi0,
                          psi=args.psi,
                          alpha=args.alpha,
                          delta=args.delta,
                          det_name=detector,
                          tref_at_det=True,
                          extra_comment = cmdline
                        )
        t_sig = time()-t1
        print("%s: generated signal for detector '%s'" % (out_dir, detector))

        # sanity check strain
        t, h = S.get_strain(128, noise_sqrt_Sh=args.noise_sqrt_Sh)
        if min(h) >= 0.0 or max(h) <= 0.0:
            print("%s: invalid strain range %g to %g\n" % (out_dir, min(h), max(h)))
            sys.exit(1)

        t_fr = 0
        t_sfts = 0

        # write frames
        if args.frames:
            frame_out_dir = os.path.join(out_dir, 'frames')
            if not os.path.isdir(frame_out_dir):
                os.makedirs(frame_out_dir)
            t2 = time()
            for path, i, N in S.write_frame_files(fs=args.frame_fs, Tframe=args.frame_T, comment="vanPuttenWaveform", noise_sqrt_Sh=args.noise_sqrt_Sh, out_dir=frame_out_dir):
                pass
            t_fr = time()-t2
            print("%s: generated frames for detector '%s'" % (out_dir, detector))

        # write SFTs
        if args.sfts:
            t3 = time()
            sft_out_dir = os.path.join(out_dir, 'sfts')
            if not os.path.isdir(sft_out_dir):
                os.makedirs(sft_out_dir)
            for path, i, N in S.write_sft_files ( fmax=args.sft_fmax,
                                                  Tsft=args.sft_T,
                                                  comment="vanPuttenWaveform",
                                                  noise_sqrt_Sh=args.noise_sqrt_Sh,
                                                  out_dir=sft_out_dir,
                                                  window=args.sft_window,
                                                  window_param=args.sft_window_param
                                                ):
                pass
            t_sfts = time()-t3
            print("%s: generated SFTs for detector '%s'" % (out_dir, detector))

        # generate stamp file
        sf = open(stamp_file, 'w')
        sf.write('Generated on %s\nby %s\nwith arguments:\n%s\n' % (datetime.datetime.now(), sys.argv[0], args_str))
        sf.close()

        print("%s: Took %.2fs for detector '%s': %.2f for CWSimulator, %.2f for frames, %.2f for SFTs" % (out_dir, time()-t0, detector, t_sig, t_fr, t_sfts))

# make waveform data
params = itertools.product(args.f0, args.ts, args.fs, args.tau, args.eps, args.dd, args.cosi)
if args.threads == 1:
    for p in params:
        make_van_putten_data(p)
else:
    pool = multiprocessing.Pool(args.threads)
    try:
        pool.map_async(make_van_putten_data, params).get(864000)
    except KeyboardInterrupt:
        pool.terminate()
        sys.exit(1)
    pool.close()
    pool.join()
